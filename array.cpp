#include <bits/stdc++.h>
using namespace std;

void twoDimensional(int **array, int N)
{

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << **array << " ";
            array++;
        }
        cout << endl;
    }
}

int main()
{
    int N = 3;
    int **array;
    array = new int*[N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cin >> array[i][j];
        }
    }

    int array2[3][3] = {{1,2,3},{4,5,6},{7,8,9}};

    twoDimensional(array, 3);
    twoDimensional(array2, 3);
    return 0;
}
#include <bits/stdc++.h>
using namespace std;

void swap(int &x, int &y);

int main()
{
    int x = 5;
    int &y = x;
    y = 4;
    cout << "y = " << y << " x = " << x << endl;
    x = 4; 
    y = 9;
    swap(x, y);
    cout << "x = " << x << " y = " << y << endl;
    return 0;
}

void swap(int &x, int &y){
    int &t = x;
    x = y;
    y = t;
    return;
}
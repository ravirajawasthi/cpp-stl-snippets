#include <bits/stdc++.h>
using namespace std;

int findSum(string str)
{
    long long unsigned int multiplier = 1, sum = 0;
    int  currentNum = -1;
    const int ascii_mapper = 48;
    int n = str.size();
    int asciiVal;

    for (int i = 0; i < n / 2; i++)
    {
        swap(str[i], str[n - i - 1]);
    }

    for (int j = 0; j < n; j++)
    {
        char i = str[j];
        asciiVal = (int)i;
        if (i >= 48 && i <= 57)
        {
            if (currentNum == -1)
            {
                currentNum = asciiVal - ascii_mapper;
                multiplier *= 10;
            }
            else
            {
                currentNum += (asciiVal - ascii_mapper) * multiplier;
                multiplier *= 10;
            }
        }
        else if ((currentNum != -1))
        {
            sum += currentNum;
            currentNum = -1;
            multiplier = 1;
        }
    }
    if(currentNum != -1) sum += currentNum;
    return sum;
}

int main()
{
    int t;
    string str;
    int res;
    cin >> t;

    for (int i = 0; i < t; i++)
    {
        cin >> str;
        res = findSum(str);
        cout << res << endl;
    }

    return 0;
}
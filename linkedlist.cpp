#include <bits/stdc++.h>
using namespace std;
struct Node
{
    int data;
    struct Node *next;

}; //*head = NULL;
struct Node *head = NULL;

void append(int val)
{
    cout << "inserting " << val << endl;
    struct Node *temp, *newnode;
    temp = head;
    //this part is for creating new node
    newnode = (struct Node *)malloc(sizeof(struct Node)); // allocating memory for new node
    newnode->data = val;                                  //assigning appropriate value to newnode
    newnode->next = NULL;                                 //next is null because it will be inserted in end
    //This part is to appropriately insert newnode
    if (head == NULL) //if linked list has no element in it
    {
        head = newnode;
    }
    else
    {
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = newnode;
    }
}
void display()
{
    struct Node *temp = head;

    while (temp != NULL)
    {
        cout << "here";
        cout << temp->data << " ";
        temp = temp->next;
    }
}

void reverse()
{
    struct Node *currNode = head;
    struct Node *prevNode = NULL;
    struct Node *nextNode = NULL;
    while (currNode->next != NULL)
    {
        nextNode = currNode->next;
        currNode->next = prevNode;

        prevNode = currNode;
        currNode = nextNode;
    }
    currNode->next = prevNode;
    head = currNode;
}

int main()
{
    int val;
    do
    {
        cin >> val;
        if (val > 0)
            append(val);
    } while (val > 0);
    reverse();
    display();
    return 0;
}

#include<bits/stdc++.h>
using namespace std;

int strstr(string s, string x);

int main(){
    int t, res;
    string str, target;
    cin >> t;   
    for (int i = 0; i < t; i++){
        cin >> str;
        cin >> target;
        res = strstr(str, target);
        cout << res;
    }
    return 0;
}

int strstr(string s, string x){
    int currIndex = -1, 
        startIndex = -1, 
        lenS = s.size(), 
        lenX = x.size();

    for (int i = 0; i < lenS; i++){
        if (s[i] == x[currIndex + 1]){
            startIndex = startIndex == -1 ? i : startIndex;
            currIndex++;
        } else if(currIndex == lenX - 1){
            break;
        } else {
            currIndex = -1;
            startIndex = -1;
        }
    }
    if (currIndex == lenX - 1) return startIndex;
    return -1;
}
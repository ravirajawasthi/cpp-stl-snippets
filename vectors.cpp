#include<iostream>
#include<bits/stdc++.h>
#include<numeric>
using namespace std;

int main(){
    int n, i = 1, sum = 0;
    cout<< "Number of elements : ";
    cin >> n;
    vector <int> array(n, 0);
    cout << "Enter all elements in array " << endl;
    
    for(auto x: array){
        cout << "Enter "<< i <<"th element : ";
        cin >> array[i - 1];
        i++;
    }
    cout << "Input complete!" << endl;

    int res =  *max_element(array.begin(), array.end());
    cout << "Maximum element is : " << res << endl;

    sum = accumulate(array.begin(), array.end(), sum);
    cout << "Sum of all elements is : " << sum << endl;
    return 0;
}
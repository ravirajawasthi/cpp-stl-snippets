#include<iostream>
#include<bits/stdc++.h>
using namespace std;

int main(){

//C styles strings===============================================================

    char str[6] = "hi";
    char str2[6] = "hi hi";
    // char str3[6] = "hihihi"; #Generates error

    cout << "size of str is : " << sizeof(str) << endl;
    cout << "size of str2 is : " << sizeof(str2) << endl;
    // cout << "size of str3 is : " << sizeof(str3);

//C++ styles strings===============================================================

    string hello;
    cout << "hello = " << hello << endl;
    cout << "Enter a value for hello : ";
    getline(cin, hello);
    cout << "hello = " << hello << endl;
    hello = "I have changed the value of hello";
    cout << "hello = " << hello << endl;
    cout << "size of hello is : " << hello.size() << endl;
    cout << "MaxSize of hello is : " << hello.max_size();
    
    return 0;
}
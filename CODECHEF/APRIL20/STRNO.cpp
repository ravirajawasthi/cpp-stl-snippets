#include <bits/stdc++.h>
using namespace std;
vector<int> nPrime(int N);
int main()
{
    int t, X, K;
    cin >> t;
    vector<int> res;
    for (int i = 0; i < t; i++)
    {
        cin >> X;
        cin >> K;
        if (K > X)
        {
            cout << 0;
        }
        else
        {
            res = nPrime(K);
            
            cout << endl;
        }
    }
    return 0;
}

vector<int> nPrime(int N)
{
    int marked[N + 1] = {0};
    int j;
    int restPrime = false;
    vector<int> res;
    for (int i = 2; i <= N; i++)
    {
        if (marked[i] == 0)
        {
            res.push_back(i);
            j = i * i;
            if (j > N)
            {
                for (int k = i + 1; k <= N; k++)
                {
                    if (marked[k] == 0)
                    {
                        res.push_back(k);
                    }
                }
                break;
            }
            else
            {
                for (int k = j; k <= N; k += i)
                {
                    marked[k] = 1;
                }
            }
        }
    }
    return res;
}
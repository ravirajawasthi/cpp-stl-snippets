#include <bits/stdc++.h>

using namespace std;

char *encode(char *src);

int main()
{
    int t, j = 0;
    string s;
    cin >> t;
    for (int i = 0; i < t; i++)
    {
        cin >> s;
        char arr[s.size() * 2 + 1];
        
        while (s[j] != '\0')
        {
            arr[j] = s[j];
            j++;
        }
        arr[j] = '\0';
        const char *res = encode(arr);
        j = 0;
        while(res[j]!='\0'){
            cout << res[j++];
        }
        cout << endl;
        j = 0;
    }
    return 0;
}

char *encode(char *src){
    char curr=src[0];
    char prev=src[0];

    // METHOD 1
    char *res=(char*)malloc(sizeof(char)*100*2);
    

    //METHOD 2
    // static char res[201] = {'\0'};
    // for (int i = 0; i < 201; i++) res[i] = '\0';
    
    int currCount = 0, j = 0;
    int currIndex = 0;

    while(src[j] != '\0'){
        curr = src[j];
        if (curr != prev){
            res[currIndex++] = prev;
            res[currIndex++] = (char)(currCount + '0');
            currCount = 1;
        } else {
            currCount++;
        }
        prev = curr;
        j++;
    }
    res[currIndex++] = curr;
    res[currIndex++] = (char)(currCount + '0');
    return res;
}

char *encode(char *src){
    char curr=src[0];
    char prev=src[0];
    static char res[53] = {'\0'};
    int currCount = 0, j = 0;
    int currIndex = 0;

    while(src[j] != '\0'){
        curr = src[j];
        if (curr != prev){
            res[currIndex++] = prev;
            res[currIndex++] = (char)(currCount + '0');
            currCount = 1;
        } else {
            currCount++;
        }
        prev = curr;
        j++;
    }
    res[currIndex++] = curr;
    res[currIndex++] = (char)(currCount + '0');
    return res;
}
#include <bits/stdc++.h>
using namespace std;

bool areKAnagrams(string, string, int);

int main()
{
    int k, t;
    bool res;
    string s, x;
    cin >> t;
    for (int i = 0; i < t; i++)
    {
        cin >> s;
        cin >> x;
        cin >> k;
        res = areKAnagrams(s, x, k);
        cout << res;
        cout << endl;
    }
    return 0;
}

bool areKAnagrams(string str1, string str2, int k)
{
    const int sizeStr1 = str1.size();
    const int sizeStr2 = str2.size();
    const char *str1Val = str1.c_str();
    const char *str2Val = str2.c_str();

    int diff = 0;
    if (sizeStr1 != sizeStr2)
    {
        return false;
    }
    else if (str1 == str2)
    {
        return true;
    }
    else
    {
        int charCountStr1[26] = {0};
        int charCountStr2[26] = {0};

        for (int i = 0; i < sizeStr1; i++)
        {
            charCountStr1[str1[i] - 'a'] += 1;
            charCountStr2[str2[i] - 'a'] += 1;
        }

        for(int i = 0; i < 26; i++){
            if (charCountStr1[i] > charCountStr2[i]) diff += abs(charCountStr1[i] - charCountStr2[i]);
            
            if (diff > k) return false;
        }
    }
    return true;
}

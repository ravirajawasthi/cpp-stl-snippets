#include <bits/stdc++.h>
using namespace std;

int main()
{
    union test {
        int a;
        char bin[sizeof(int)];
    };
    test obj;
    obj.a = 1024;
    cout << "binary output is : ";
    for (int i = 0; i < sizeof(int); i++)
        cout << (int)obj.bin[i];
    cout << endl;
    return 0;
}
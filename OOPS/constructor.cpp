#include <bits/stdc++.h>
using namespace std;
class Test
{
private:
    int t;

public:
    Test(int t = 0) : t(t) { cout << "Initialized List constructor" << endl; }
    int getVal()
    {
        return t;
    }
};
class Test2
{
private:
    int t = 99;

public:
    int getVal()
    {
        return t;
    }
};

class pointerClass
{
private:
    int t = 0;
    int *ptr = &t;

public:
    pointerClass()
    {
        t = 0;
        int *ptr = &t;
    }
    pointerClass(const pointerClass &gg)
    {
        this->t = gg.t;
        this->ptr = &(this->t);
    }
    int *getAddress() { return ptr; }
    int getVal() { return *ptr; }
};

class Demo
{
private:
    int x = 0;
    int y = 0;

public:
    Demo(int x = 0, int y = 0)
    {
        this->x = x;
        this->y = y;
    }
    int getX()
    {
        return this->x;
    }
    int getY()
    {
        return this->y;
    }
};

int main()
{
    Test t1(100);
    Test t2;
    cout << "t1 has a value of : " << t1.getVal() << endl;
    // cout << "t1 has a value of : " << t1.t << endl; Creates a error
    cout << "t2 has a value of : " << t2.getVal() << endl;
    t2.getVal();
    Test2 t3;
    cout << "t3 has a value of : " << t3.getVal() << endl;
    cout << "t3 has a value of : " << t3.getVal() << endl;
    cout << "t3 has a value of : " << t3.getVal() << endl;

    pointerClass t4;
    pointerClass t5(t4);
    int *temp = t4.getAddress();
    *temp = 50;
    cout << "t4 has a value of : " << t4.getVal() << endl;
    cout << "t5 has a value of : " << t5.getVal() << endl;
    return 0;
}
#include <bits/stdc++.h>
using namespace std;

class Complex
{
private:
    int real;
    int img;

public:
    Complex(int i, int r)
    {
        real = i;
        img = r;
    }
    void print()
    {
        cout << real << " + " << img << "i";
    }
};

int main()
{   
    Complex c1(1, 2);
    c1.print();
    return 0;
}
#include <bits/stdc++.h>
using namespace std;

vector<string> findMatchedWords(vector<string> dict, string pattern);
vector<int> encode(string);

int main()
{
    signed s;
    unsigned u;
    long l;
    long long ll;
    int t, n;
    string str;
    string patten;
    vector<string> dict;
    cin >> t;
    for (int i = 0; i < t; i++)
    {
        cin >> n;
        for (int j = 0; j < n; j++)
        {
            cin >> str;
            dict.push_back(str);
        }
        cin >> patten;
        vector<string> res = findMatchedWords(dict, patten);
        for (string s : res)
        {
            cout << s << " ";
        }
        cout << endl;
    }
    return 0;
}

vector<string> findMatchedWords(vector<string> dict, string pattern)
{
    vector<string> output;
    vector<int> tempOutput, encodedPattern = encode(pattern);
    for (string str : dict)
    {
        tempOutput = encode(str);
        if (tempOutput == encodedPattern)
        {
            output.push_back(str);
        }
    }
    return output;
}

vector<int> encode(string str)
{
    int seen[127] = {0};
    vector<int> res(str.size(), 0);
    int uniqueChar = 0;
    for (int i = 0; i < str.size(); i++)
    {
        int charVal = str[i];
        if (seen[charVal] == 0)
        {
            seen[charVal] = ++uniqueChar;
            res[i] = seen[charVal];
        }
        else
        {
            res[i] = seen[charVal];
        }
    }
    return res;
}
#include<bits/stdc++.h>
using namespace std;

int main(){
    // get some bits from a number
    int a = 0b101111;
    int b = a & 0b110000; // b = 100000 hence 6th and 5th bit from right 
    cout << b << endl; 

    // get if a particular bit is active
    cout << ((a & (1 << 4)) > 0 ? "Active" : "Off") << endl; // check if 5th bit from right is active

    //set bits of a number
    int c = 0b00000000; // set 3 bit in center to a value b/w 0 and 7
    int y = 4;
    c |= (y << 3);
    cout << c << endl;

    //set a particular bit to 0
    c = 0b0001000;
    c = c & ~(1 << 3);
    cout << c << endl;

    //Toggle a particular bit
    c = c ^ (1 << 3);
    cout << c << endl;

    return 0;
}